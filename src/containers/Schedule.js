import React, {Component} from 'react';
// import logo from '../assets/logo.svg';
import './Schedule.css';
import {SideBar} from "../compopnents/SideBar/SideBar";
import {SegmentedControl} from "antd-mobile";

class Schedule extends Component {

  render() {
    return (
      <div className="Schedule">
        <SideBar />
        <p>View</p>
        <SegmentedControl
          // renderSegmentItem={(idx, value, selected)=> {
          //   return <div style={{
          //     backgroundColor: selected ? '#c33d51' : '#cccccc',
          //     borderColor: selected ? '#c33d51' : '#cccccc',
          //     color: selected ? 'transparent' : '#000000'
          //   }}>{value}</div>
          // }}
          className={'periodTabs'}
          tintColor={'#cccccc'}
          selectedIndex={1}
          values={['Year', 'Month', 'Week', 'Day']}
        />
      </div>
    );
  }
}

export default Schedule;
