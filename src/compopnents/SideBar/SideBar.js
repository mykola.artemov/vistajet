import React, {Component} from "react";
import {Drawer, Icon, List, NavBar} from "antd-mobile";
import SVGInline from "react-svg-inline";
import "./SideBar.css";

const logo = require('../../assets/logo.png');
const closeIcon = require('../../assets/icons/close.svg');
const hamburger = require('../../assets/icons/hamburger.svg');
const Item = List.Item;

export class SideBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
    this.onOpenChange = this.onOpenChange.bind(this);
  }

  closeSideBar = () => {
    this.setState({open: false});
  };

  menu = <List>
    <Item key="close" thumb={<SVGInline width={'18px'} svg={closeIcon}/>}
          extra={<img className="logo" src={logo} alt="logo"/>} onClick={this.closeSideBar}/>
    <Item key="home">
      <Icon type="appstore"/>Home
    </Item>
    <Item key="personal">
      <Icon type="appstore"/>Personal
    </Item>
    <Item key="myTeam">
      <Icon type="appstore"/>My Team
      <List >
        <Item key="setting:1">Team members</Item>
        <Item key="setting:2">Team vocations</Item>
      </List>
    </Item>
    <Item key="alipay">
      <a href="https://ant.design" target="_blank" rel="noopener noreferrer">Navigation Four - Link</a>
    </Item>
  </List>;

  onOpenChange() {
    this.setState({open: !this.state.open});
  };

  render() {
    const {open} = this.state;
    return <div className="SideBar">
      <NavBar mode={'light'} icon={<SVGInline className="menu-hamburger-button" svg={hamburger}/>} onLeftClick={this.onOpenChange}/>
      <Drawer
        className="my-drawer"
        drawerWidth={250}
        style={{minHeight: document.documentElement.clientHeight}}
        enableDragHandle
        contentStyle={{color: '#A6A6A6', textAlign: 'center', paddingTop: 42}}
        sidebar={this.menu}
        open={open}
        onOpenChange={this.onOpenChange}
      >
        <div/>
      </Drawer>
    </div>
  }
}

// SideBar.propTypes = {};
