import React from 'react';
import ReactDOM from 'react-dom';
import 'antd-mobile/dist/antd-mobile.css';
import './index.css';
import Schedule from './containers/Schedule';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Schedule />, document.getElementById('root'));
registerServiceWorker();
